module RL where

import Data.Array
import System.Random
import Numeric
import Data.List
import Data.List.Split(chunksOf)

{-
    O stare este reprezentată ca un număr întreg.
    O cale este o listă de stări, eventual infinită.

    O estimare reprezintă o mulțime de asocieri (stare, informație aferentă).
    Cu toate că mediul este bidimensional, utilizăm o reprezentare liniară
    a acestuia, bazată pe un `Array`, cu stările indexate ca în figura din 
    enunț.
-}
type State      = Int
type Path       = [State]
type Estimation = Array State Float

{-
    Lățimea și înălțimea mediului, precum și numărul de stări.
-}
width, height, nStates :: Int
width   = 4
height  = 3
nStates = width * height

{-
    Perechile de stări vecine.
-}
neighbors :: [(State, State)]
neighbors = [ (1, 2), (1, 5)
            , (2, 1), (2, 3)
            , (3, 2), (3, 4)
            , (3, 7)
            , (4, 3), (4, 8)
            , (5, 1), (5, 9)
            , (7, 3), (7, 8), (7, 11)
            , (8, 4), (8, 7), (8, 12)
            , (9, 5), (9, 10)
            , (10, 9), (10, 11)
            , (11, 7), (11, 10), (11, 12)
            , (12, 8), (12, 11)
            ]

{-
    Starea de pornire.
-}
startState :: State
startState = 1

{-
     Stările terminale.
-}
terminalStates :: [State]
terminalStates = [8, 12]

{-
    Rata de învățare alfa și factorul de scalare a acesteia.
-}
learningRate, scaleFactor :: Float
learningRate = 0.1
scaleFactor  = 0.999

-------------------------------------------------------------------------------
-- Completați sub această linie.


--  === 1. Generarea căilor ===

{-
    *** TODO ***

    Întoarce toate stările vecine ale unei stări.
-}
neighborsOf :: State -> [State]
neighborsOf state = map snd $ filter (\(x, y) -> x == state) neighbors

{-
    *** TODO ***

    Construiește o cale aleatoare infinită, pe baza unui generator.

    Hint: `Data.List.iterate`, `System.Random.split`.
-}

getSGPair :: RandomGen g => (State, g) -> (State, g)
getSGPair pair = (neighborsOf (fst pair) !! head (randomRs (0, length (neighborsOf (fst pair)) - 1) (fst . split $ (snd pair))), fst . split $ (snd pair))


randomPath :: RandomGen g => g -> (Path, g)
randomPath g = (map fst (iterate getSGPair (startState, fst . split $ g)), snd . split $ g)

{-
    *** TODO ***

    Trunchiază o cale, eventual infinită, la prima stare terminală.
-}

terminatePath :: Path -> Path
terminatePath path = take len path
    where
      len = length (takeWhile (\element -> not $ elem element [x | x <- terminalStates]) path ) + 1

{-
    *** TODO ***

    Construiește o infinitate de căi infinite.
-}

randomPaths :: RandomGen g => g -> [Path]
randomPaths g = map fst (iterate (\(path, gen) -> randomPath $ (snd . split) $ gen) (fst . randomPath $ g, g))

--  === 2. Estimarea utilităților fără diminuarea ratei de învățare ===

{-
    *** TODO ***

    Array cu conscințele specifice fiecărei stări.
-}
reinforcements :: Array State Float
reinforcements = array (1, 12) [(1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (8, -1), (9, 0), (10, 0), (11, 0), (12, 1)]

{-
    *** TODO ***

    Valorile inițiale ale stărilor, înaintea rulării algoritmului.
    Se construiesc pe baza array-ului de consecințe.
-}
initialEstimation :: Estimation
initialEstimation = reinforcements

{-
    *** TODO ***

    Lista de utilități provenite dintr-o estimare.
-}
values :: Estimation -> [Float]
values estim = map snd $ assocs estim

{-
    *** TODO ***

    Reprezentarea sub formă de șir de caractere a unei estimări.
    Se va întrebuința forma bidimensională, ca în imaginile din enunț.
    De asemenea, utilitățile vor fi rotunjite la 2 zecimale, și vor
    avea semnul inclus.

    Hint: `Text.Printf`.

    Exemplu de rezultat:

    -0.07 +0.06 +0.20 +1.00
    -0.20 +0.00 -0.43 -1.00
    -0.32 -0.45 -0.56 -0.78

    Pentru a vizualiza corect caracterele de linie nouă, aplicați
    în interpretor funcția `putStrLn` asupra șirului obținut.
-}

twoDecimals :: RealFloat a =>  a -> String
twoDecimals value = showFFloat (Just 2) value ""

myLines :: Estimation -> [[String]]
myLines estim = map (map twoDecimals) (map (map snd) (reverse $ chunksOf width $ assocs estim))

toString :: Estimation -> String
toString estim = show $ myLines estim

strip :: String -> String -> String
strip toremove input = filter (flip notElem toremove) input

myReplace :: String -> String -> String -> String
myReplace [] _ _ = []
myReplace str what new =
    if take (length what) str /= what
        then [head str] ++ (myReplace (tail str) what new)
        else new ++ myReplace (drop (length what) str) what new

myTrim :: String -> String
myTrim str = strip "]" $ myReplace (myReplace (myReplace ("+" ++ (strip "[\"" str)) "]," "\n+") "," " +") "+-" "-"

showEstimation :: Estimation -> String
showEstimation estim = myTrim $ toString estim

{-
    *** TODO ***

    Actualizează o estimare în urmare parcurgerii unei căi.

    Hint: `Data.Array.accum`.
-}
updateState :: Estimation -> State -> State -> Estimation
updateState e curr next = accum (+) e [(curr, learningRate * (e ! next - e ! curr))]

updateEstimation :: Estimation -> Path -> Estimation
updateEstimation e path =
            if (length . tail) path == 0 then e
            else updateEstimation (updateState e (head path) ((head . tail) path)) (tail path)

{-
    *** TODO ***

    Obține un flux infinit de estimări rafinate succesiv, pe baza unui flux
    infinit de căi finite, încheiate în stări terminale.

    Hint: `Data.List.mapAccumL`.
-}

estimations :: [Path] -> [Estimation]
estimations paths = snd $ mapAccumL (\estim path -> (updateEstimation estim path, updateEstimation estim path)) initialEstimation paths

{-
    *** TODO ***

    Determină estimarea de rang dat ca parametru, pe baza unui generator.
-}

terminatePaths :: RandomGen g => g -> [Path]
terminatePaths g = map terminatePath $ randomPaths g

estimate :: RandomGen g => Int -> g -> Estimation
estimate i g = (estimations (terminatePaths g)) !! i

{-
    *** TODO ***

    Pentru o stare, determină vecinul cu cea mai mare valoare estimată.

    Hint: `Data.Function.on`.
-}

-- Dandu-se o estimare, se alege valoarea estimata maxima

bestFromEstimation :: Estimation -> Float
bestFromEstimation estim = vals !! length (takeWhile (/= maxValue) vals)
      where
        vals = map snd $ assocs estim
        maxValue = maximum vals

-- Se construieste un array cu vecinii elementului curent si valorile
-- estimate ale acestora

neighborsEstims :: State -> Estimation -> Estimation
neighborsEstims curr estim = array (1, length (neighborsOf curr)) [(i, estim ! (neighborsOf curr !! (i - 1))) | i <- [1..length (neighborsOf curr) ]]

-- Verificam care vecin are valoarea estimata maxima

bestNeighborOf :: State -> Estimation -> State
bestNeighborOf curr estim = fst $ head $ filter (\(x, y) -> y == maxVal) (assocs estim)
  where
    maxVal = bestFromEstimation $ neighborsEstims curr estim

{-
    *** TODO ***

    Contruiește o cale începută în starea inițială, pe principiul alegerii 
    vecinului cu utilitata maximă.
-}

bestPath :: Estimation -> Path
bestPath estim = iterate (\curr -> bestNeighborOf curr estim) startState


--  === 3. Estimarea utilităților cu diminuarea ratei de învățare ===

{-
    *** TODO ***

    Fluxul infinit al ratelor de învățare scalate:

    [ 1
    , learningRate
    , learningRate * scaleFactor
    , learningRate * scaleFactor^2
    , ...
    ]
-}
scaledLearningRates :: [Float]
scaledLearningRates = undefined

{-
    *** TODO ***

    Tip de date pentru reținerea atât a valorii estimate a unei stări,
    cât și a numărului de vizitări ale acesteia.
-}
data StateInfo = Todo
    deriving (Show)
